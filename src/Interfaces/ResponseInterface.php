<?php
/**
 * OmniShipp
 * Copyright (c) 2020 ThemePoint
 *
 * @author Hendrik Legge <hendrik.legge@themepoint.de>
 * @version 1.0.0
 * @package omnishipp.client.core
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Omnishipp\Interfaces;

interface ResponseInterface
{
    /**
     * Return the model class.
     *
     * @return ModelInterface
     */
    public function getModel(): ModelInterface;

    /**
     * Set the data.
     *
     * @param ModelInterface $model
     */
    public function setData(ModelInterface $model): void;

    /**
     * Return the data.
     *
     * @return ModelInterface
     */
    public function getData(): ModelInterface;

    /**
     * Set the response code.
     *
     * @param int $responseCode
     */
    public function setResponseCode(int $responseCode): void;

    /**
     * Return the response code.
     *
     * @return int
     */
    public function getResponseCode(): int;
}
