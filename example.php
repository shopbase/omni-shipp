<?php

use Http\Adapter\Guzzle6\Client as HttpClient;

$httpClient = HttpClient::createWithConfig([
    'base_uri' => 'https://example.com',
    'auth' => [
        '<<username>>',
        '<<password>>',
    ],
    'connect_timeout' => 60,
    'timeout' => 120
]);

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

$encoders = [new XmlEncoder(), new JsonEncoder()];
$normalizers = [new ObjectNormalizer()];
$serializer = new Serializer($normalizers, $encoders);

$client = new \ExampleClient($httpClient, $serializer);

$model = new \ExampleModel();

$response = $client->executeEndpoint(\ExampleEndpoint(new \ExampleEndpoint($model)), [], [], []);

// Code to process response is running here