<?php
/**
 * OmniShipp
 * Copyright (c) 2020 ThemePoint
 *
 * @author Hendrik Legge <hendrik.legge@themepoint.de>
 * @version 1.0.0
 * @package omnishipp.client.core
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Omnishipp\Interfaces;

interface EndpointInterface
{
    /**
     * EndpointInterface constructor.
     *
     * @param ModelInterface $body
     * @param array          $headerParameters
     */
    public function __construct(ModelInterface $body, array $headerParameters = array());

    /**
     * Return the HTTP Method.
     *
     * @return string
     */
    public function getMethod(): string;

    /**
     * Return the Uri of Endpoint.
     *
     * @return string
     */
    public function getUri(): string;

    /**
     * Return the extra headers of endpoint.
     *
     * @return array
     */
    public function getExtraHeaders(): array;

    /**
     * Return the body.
     *
     * @return ModelInterface
     */
    public function getBody(): ModelInterface;

    /**
     * Set the body.
     *
     * @param ModelInterface $model
     */
    public function setBody(ModelInterface $model): void;

    /**
     * Return the normalizer.
     *
     * @return NormalizerInterface
     */
    public function getNormalizer(): NormalizerInterface;

    /**
     * Return the serialisationType.
     *
     * @return string
     */
    public function getSerialisationType(): string;

    /**
     * Return the response model.
     *
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface;
}
