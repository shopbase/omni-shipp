<?php
/**
 * OmniShipp
 * Copyright (c) 2020 ThemePoint
 *
 * @author Hendrik Legge <hendrik.legge@themepoint.de>
 * @version 1.0.0
 * @package omnishipp.client.core
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Omnishipp;

use Omnishipp\Interfaces\DataInterfaces\NormalizedDataInterface;

class NormalizedData implements NormalizedDataInterface
{
}
