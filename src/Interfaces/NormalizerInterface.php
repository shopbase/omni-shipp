<?php
/**
 * OmniShipp
 * Copyright (c) 2020 ThemePoint
 *
 * @author Hendrik Legge <hendrik.legge@themepoint.de>
 * @version 1.0.0
 * @package omnishipp.client.core
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Omnishipp\Interfaces;

use Omnishipp\Interfaces\DataInterfaces\NormalizedDataInterface;
use Omnishipp\NormalizedData;

interface NormalizerInterface
{
    /**
     * Denormalize the data.
     *
     * @param NormalizedData $data
     * @param ModelInterface $model
     *
     * @return ModelInterface
     */
    public function denormalize(NormalizedData $data, ModelInterface $model): ModelInterface;

    /**
     * Normalize the data.
     *
     * @param ModelInterface $model
     *
     * @return NormalizedDataInterface
     */
    public function normalize(ModelInterface $model): NormalizedDataInterface;

    /**
     * Return if normalizer support denormalisation for response.
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    public function supportsDenormalization(ResponseInterface $response): bool;

    /**
     * Return if normalizer supports normalization for model.
     *
     * @param ModelInterface $model
     *
     * @return bool
     */
    public function supportsNormalization(ModelInterface $model): bool;
}
