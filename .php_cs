<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__.'/src',
    ])
;

$header = <<<'EOF'
OmniShipp
Copyright (c) 2020 ThemePoint

@author Hendrik Legge <hendrik.legge@themepoint.de>
@version 1.0.0
@package omnishipp.client.core

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
EOF;

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony' => true,
        '@Symfony:risky' => true,
        '@PHP71Migration:risky' => true,
        '@DoctrineAnnotation' => true,
        'array_syntax' => ['syntax' => 'long'],
        'combine_consecutive_issets' => true,
        'combine_consecutive_unsets' => true,
        'compact_nullable_typehint' => true,
        'concat_space' => ['spacing' => 'one'],
        'list_syntax' => ['syntax' => 'short'],
        'no_null_property_initialization' => true,
        'no_short_echo_tag' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'ordered_class_elements' => true,
        'ordered_imports' => true,
        'phpdoc_add_missing_param_annotation' => ['only_untyped' => true],
        'phpdoc_order' => true,
        'yoda_style' => null,
        'fopen_flags' => ['b_mode' => true],
        'declare_strict_types' => true,
        'header_comment' => ['header' => $header, 'separate' => 'bottom', 'location' => 'after_open', 'comment_type' => 'PHPDoc']
    ])
    ->setFinder($finder)
;
