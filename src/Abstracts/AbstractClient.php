<?php
/**
 * OmniShipp
 * Copyright (c) 2020 ThemePoint
 *
 * @author Hendrik Legge <hendrik.legge@themepoint.de>
 * @version 1.0.0
 * @package omnishipp.client.core
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Omnishipp\Abstracts;

use Guzzle\Http\ClientInterface as HttpClientInterface;
use Omnishipp\Exceptions\NormalizeException;
use Omnishipp\Interfaces\ClientInterface;
use Omnishipp\Interfaces\EndpointInterface;
use Omnishipp\Interfaces\ModelInterface;
use Omnishipp\Interfaces\ResponseInterface;
use Omnishipp\NormalizedData;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractClient implements ClientInterface
{
    protected $httpClient;

    protected $serializer;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    public function executeEndpoint(EndpointInterface $endpoint, array $headerParameters = array(), array $requestOptions = array(), array $context = array()): ResponseInterface
    {
        $normalizer = $endpoint->getNormalizer();

        if (!$normalizer->supportsNormalization($endpoint->getBody())) {
            throw new NormalizeException(sprintf('Normalizer %s do not support normalisation of Model %s', \get_class($normalizer), \get_class($endpoint->getBody())));
        }

        $normalizedData = $normalizer->normalize($endpoint->getBody());

        $serializedData = $this->serializer->serialize($normalizedData, $endpoint->getSerialisationType(), $context);

        $httpResponse = $this->httpClient->createRequest(
            $endpoint->getMethod(),
            $endpoint->getUri(),
            array_merge($endpoint->getExtraHeaders(), $headerParameters),
            $serializedData,
            $requestOptions
        )->send();

        if (!$normalizer->supportsDenormalization($endpoint->getResponse())) {
            throw new NormalizeException(sprintf('Normalizer %s do not support denormalisation of response %s', \get_class($normalizer), \get_class($endpoint->getResponse())));
        }

        $response = $endpoint->getResponse();

        $deserializedData = $this->serializer->deserialize($httpResponse->getBody(), NormalizedData::class, $endpoint->getSerialisationType());

        $denormalizedData = $normalizer->denormalize($deserializedData, $response->getModel());

        return $this->processResponse($response, $denormalizedData, $httpResponse->getStatusCode());
    }

    private function processResponse(ResponseInterface $response, ModelInterface $model, int $responseCode): ResponseInterface
    {
        $response->setData($model);
        $response->setResponseCode($responseCode);

        return $response;
    }
}
