<?php
/**
 * OmniShipp
 * Copyright (c) 2020 ThemePoint
 *
 * @author Hendrik Legge <hendrik.legge@themepoint.de>
 * @version 1.0.0
 * @package omnishipp.client.core
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Omnishipp\Interfaces;

use Guzzle\Http\ClientInterface as HttpClientInterface;
use Symfony\Component\Serializer\SerializerInterface;

interface ClientInterface
{
    /**
     * ClientInterface constructor.
     *
     * @param HttpClientInterface $httpClient
     * @param SerializerInterface $serializer
     */
    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer);

    /**
     * @param EndpointInterface $endpoint
     * @param array             $headerParameters
     * @param array             $requestOptions
     * @param array             $context
     *
     * @return ResponseInterface
     */
    public function executeEndpoint(EndpointInterface $endpoint, array $headerParameters = array(), array $requestOptions = array(), array $context = array()): ResponseInterface;
}
