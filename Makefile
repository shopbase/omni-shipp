.PHONY: cs stan test

cs: src
	php-cs-fixer fix --ansi --diff --dry-run --verbose

cs-replace: src
	php-cs-fixer fix --ansi --diff --verbose

stan: src
	php-stan analyse --autoload-file=vendor/autoload.php --configuration=phpstan.neon --level=max ./src